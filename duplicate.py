import threading

from json.decoder import JSONDecodeError
import serial
import cv2
import numpy as np
import threading
import time
import inspect
import json
import re
import math
from datetime import datetime
import signal
from dataclasses import dataclass

def eulerAnglesToRotationMatrix(theta):
    R_x = np.array([[1,         0,                  0                   ],
                    [0,         math.cos(theta[0]), -math.sin(theta[0]) ],
                    [0,         math.sin(theta[0]), math.cos(theta[0])  ]
                    ])
    R_y = np.array([[math.cos(theta[1]),    0,      math.sin(theta[1])  ],
                    [0,                     1,      0                   ],
                    [-math.sin(theta[1]),   0,      math.cos(theta[1])  ]
                    ])
    R_z = np.array([[math.cos(theta[2]),    -math.sin(theta[2]),    0],
                    [math.sin(theta[2]),    math.cos(theta[2]),     0],
                    [0,                     0,                      1]
                    ])
    R = np.dot(R_z, np.dot( R_y, R_x ))
    return R

@dataclass
class Method:
    Enable: bool
    Record: bool
    Index: int
    Width: int
    Height: int
    Tags: list()
ADAS_TAGS = ["LDW", "FCW", "Tracker", "ReactionTime"]
DMS_TAGS = ["DMS"]
METHOD_DICT = {"ADAS": Method(False, False, -1, 1280, 720, ADAS_TAGS), \
               "DMS": Method(True, False, 2, 1280, 720, DMS_TAGS)}
COM_PORT = '/dev/ttyUSB0'
BAUD_RATES = 115200*2
EXIT_EVENT = threading.Event()

class Serial_thread:
    thread = None
    datas_dict = dict()
    timestamp_dict = dict()
    mutex = threading.Lock()

    def __init__(self, com_port, baud_rates):
        self.com_port = com_port
        self.baud_rates = baud_rates
        self.thread = threading.Thread(target = self.job)

    def __del__(self):
        if self.thread.is_alive():
            self.mutex.release()
            self.thread.join()

    def start(self):
        self.running = True
        self.thread.start()

    def stop(self):
        self.running = False

    def getData(self):
        self.mutex.acquire()
        datas = self.datas_dict
        timestamp = self.timestamp_dict
        self.mutex.release()
        return datas, timestamp

    def job(self):
        serial_ = serial.Serial(self.com_port, self.baud_rates)
        while self.running:
            if EXIT_EVENT.is_set():
                break

            try:
                raw_data = serial_.readline()
                decode_data = raw_data.decode().replace("\n", "")
                datas = re.split('(\{.*?\})(?= *\{)', decode_data)
                for data in datas:
                    if data == '':
                        continue
                    json_obj = json.loads(data)

                    for key, val in json_obj.items():
                        if key != "FUNC" and 'FUNC' in json_obj:
                            key_ = json_obj['FUNC']+"-"+key
                            self.mutex.acquire()
                            self.datas_dict[key_] = val
                            self.timestamp_dict[key_] = int(round(time.time() * 1000))
                            self.mutex.release()
            except serial.serialutil.SerialException:
                continue
            except UnicodeDecodeError as e:
                continue
            except JSONDecodeError as je:
                continue

class VideoProcess:
    capture = None
    recorder = None

    def __init__(self, category, camera_index, frame_width, frame_height, enable_record, tags):
        self.category = category
        self.tags = tags
        self.width = frame_width
        self.height = frame_height

        if enable_record:
            self.enableRecord()

        if camera_index >= 0:
            self.capture = cv2.VideoCapture(camera_index)
            self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
            self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)

    def __del__(self):
        if self.recorder is not None:
            self.recorder.release()

    def enableRecord(self):
        now = datetime.now()
        date_time = now.strftime("%Y%m%d_%H%M%S")
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.recorder = cv2.VideoWriter('{}_{}.mp4'.format(self.category, date_time), fourcc, 30.0, (self.width, self.height))

    def read_and_draw(self, y_offset, data_dict, timestamp_dict):
        frame = np.zeros((self.height, self.width, 3))
        if self.capture is not None:
            ret, frame = self.capture.read()
            if not ret:
                return None, y_offset

        frame, y_offset = self.__draw(frame, y_offset, data_dict, timestamp_dict)

        if self.recorder is not None:
            self.recorder.write(frame)

        return frame, y_offset

    def show(self, frame):
        cv2.imshow(self.category, frame)

    def captureFrame(self, frame):
        print("capture!")
        now = datetime.now()
        date_time = now.strftime("%Y%m%d_%H%M%S")
        cv2.imwrite('{}.png'.format(date_time), frame)

    def __draw(self, frame, y_offset, data_dict, timestamp_dict):
        content = "------ {} -----".format(self.category)
        self.__draw_log(frame, y_offset, content, (255,0,0))
        y_offset += 20

        for tag in self.tags:
            for key, val in data_dict.items():
                if tag in key:
                    needUpdate = self.__is_update_in_second(key, timestamp_dict)
                    content = "{}:No Update".format(key)
                    if needUpdate and val is not None:
                        content = "{}:{}".format(key, val)
                        frame = self.__draw_each_method(frame, key, val)
                    self.__draw_log(frame, y_offset, content, (255,255,255))
                    y_offset += 20
        return frame, y_offset

    def __is_update_in_second(self, key, timestamp_dict, threshold_ms=1000):
        if key not in timestamp_dict:
            return False
        delta = int(round(time.time() * 1000)) - timestamp_dict[key]
        return delta <= threshold_ms

    def __draw_log(self, frame, offset, content, color):
        cv2.putText(frame, content, (0, offset), cv2.FONT_HERSHEY_PLAIN, 1, color)
        return frame

    def __draw_each_method(self, frame, key, value):
        if "LDW" in key and ("left" in key or "right" in key):
            frame = self.__draw_lane(frame, value)
        if "Tracker-object" in key or "FCW-Object" in key:
            for obj in value:
                frame = self.__draw_fcw_obj(frame, obj)
        if "DMS-phone" in key or "DMS-cigratte" in key:
            self.__draw_dms_obj(frame, value)
        if "DMS-face" in key:
            self.__draw_dms_obj(frame, value, True)
        if "DMS-headpose" in key:
            self.__draw_headpose(frame, value)
        return frame

    def __draw_fcw_obj(self, frame, obj):
        height, width, _ = frame.shape
        x = int(obj['p']['x'] * width)
        y = int(obj['p']['y'] * height)
        w = int(obj['p']['w'] * width)
        h = int(obj['p']['h'] * height)

        color = (0,0,0)
        content = ""
        if 'c' in obj and 'l' in obj:
            color = (255,0,0)
            content = str(obj['l'])+":"+str(obj['c'])
        elif 't' in obj and 'd' in obj:
            color = (0,255,0)
            content = str(obj['t'])+":"+str(obj['d'])
        cv2.rectangle(frame, (x,y), (x+w, h+y), color)
        cv2.putText(frame, content, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, color)
        return frame

    def __draw_dms_obj(self, frame, obj, isFace=False):
        height, width, _ = frame.shape
        x = int(obj['x'] * width)
        y = int(obj['y'] * height)
        w = int(obj['w'] * width)
        h = int(obj['h'] * height)

        if isFace:
            self.face = {"x":x, "y":y, "w":w, "h":h}

        color = (255,255,0)
        cv2.rectangle(frame, (x,y), (x+w, h+y), color)
        return frame

    def __draw_headpose(self, frame, headpose):
        if not hasattr(self, 'face'):
            return frame

        height, width, _ = frame.shape
        yaw = headpose['yaw']*math.pi/180
        pitch = headpose['pitch']*math.pi/180
        roll = -headpose['roll']*math.pi/180

        rotation_matrix = eulerAnglesToRotationMatrix([pitch, yaw, roll])
        cx = np.dot(rotation_matrix, (150,0,0))
        cy = np.dot(rotation_matrix, (0,-150,0))
        cz = np.dot(rotation_matrix, (0,0,130))

        center = (int(self.face["x"]+0.5*self.face["w"]), int(self.face["y"]+0.5*self.face["h"]))
        center = (-1*int(cz[0])+center[0], int(cz[1]*0.5)+center[1])

        cx = (-1*int(cx[0])+center[0], int(cx[1])+center[1])
        cy = (-1*int(cy[0])+center[0], int(cy[1])+center[1])
        cz = (-1*int(cz[0])+center[0], int(cz[1])+center[1])

        cv2.arrowedLine(frame, center, cy, (255,0,0), 2)
        cv2.arrowedLine(frame, center, cz, (0,255,0), 2)
        cv2.arrowedLine(frame, center, cx, (0,0,255), 2)
        return frame

    def __draw_lane(self, frame, lanes):
        height, width, _ = frame.shape
        x0 = int(lanes[0]['x'] * width)
        y0 = int(lanes[0]['y'] * height)
        x1 = int(lanes[1]['x'] * width)
        y1 = int(lanes[1]['y'] * height)
        x2 = int(lanes[2]['x'] * width)
        y2 = int(lanes[2]['y'] * height)
        cv2.line(frame, (x0,y0), (x1,y1), (0, 0, 255), 2)
        cv2.line(frame, (x1,y1), (x2,y2), (0, 0, 255), 2)
        return frame

def signal_handler(signum, frame):
    EXIT_EVENT.set()

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)

    # [serial]
    serial_ = Serial_thread(COM_PORT, BAUD_RATES)
    serial_.start()

    # [video]
    # create video process
    vp_list = list()
    for key, val in METHOD_DICT.items():
        if val.Enable:
            cv2.namedWindow(key)
            vp = VideoProcess(key, val.Index, val.Width, val.Height, val.Record, val.Tags)
            vp_list.append(vp)

    # loop to show frame
    key = -1
    while True:
        if EXIT_EVENT.is_set():
            break
        data_dict, timestamp_dict = serial_.getData()
        for vp in vp_list:
            y_offset = 100
            frame, y_offset = vp.read_and_draw(y_offset, data_dict, timestamp_dict)
            if frame is None:
                continue
            if key == 13:
                vp.captureFrame(frame)
            vp.show(frame)
        # cv2.waitKey(10)
        key = cv2.waitKey(90)