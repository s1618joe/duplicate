import threading

from json.decoder import JSONDecodeError
import serial
import cv2
import numpy as np
import threading
import time
import inspect
import json
import re
import math
from datetime import datetime


def retrieve_name(var):
    """
    Gets the name of var. Does it from the out most frame inner-wards.
    :param var: variable to get name from.
    :return: string
    """
    for fi in reversed(inspect.stack()):
        names = [var_name for var_name, var_val in fi.frame.f_locals.items() if var_val is var]
        if len(names) > 0:
            return names[0]

DMS_FPS = "DMS-FPS"
HEADPOSE = "DMS-HEADPOSE"
key_list = [DMS_FPS, HEADPOSE]

# COM_PORT = 'COM5'
COM_PORT = '/dev/ttyUSB1'
BAUD_RATES = 115200*2
running = True
level_interval = 30
value_dict = {}
timestamp_dict = {}

camera_is_right_of_face = True
mutex = threading.Lock()

FOCUS_ON = "DMS"

track_fcw_diff_queue = []
input_fcw_diff_queue = []
input_track_diff_queue = []
track_time_queue = []

encode_dms = False
encode_adas = False

fourcc = cv2.VideoWriter_fourcc(*'XVID')

now = datetime.now() # current date and time
date_time = now.strftime("%Y%m%d_%H%M%S")

adasOutVideo = None
dmsOutVideo = None
if encode_dms:
    dmsOutVideo = cv2.VideoWriter('dms_{}.mp4'.format(date_time), fourcc, 30.0, (1280,  720))
if encode_adas:
    adasOutVideo = cv2.VideoWriter('adas_{}.mp4'.format(date_time), fourcc, 30.0, (1280,  720))


def update_key_value(key, value):
    global value_dict, timestamp_dict, mutex
    mutex.acquire() 
    value_dict[key] = value
    timestamp_dict[key] = int(round(time.time() * 1000))
    mutex.release() 

def is_update_in_second(key, millisecond=5000):
    global timestamp_dict
    if key not in timestamp_dict:
        return False
    delta = int(round(time.time() * 1000)) - timestamp_dict[key]
    return delta <= millisecond

def parseMultipleJson(multijson):
    # print(re.findall(r'{.{33}', multijson))
    return re.findall(r'{.{33}', multijson)

def serial_job():
    global value_dict, running
    ser = serial.Serial(COM_PORT, BAUD_RATES)   
    data_raw = ser.readline()  

    try:
        while running:
            while ser.in_waiting:           
                data_raw = None
                try:
                    data_raw = ser.readline()
                    data = data_raw.decode().replace("\n","")
                    # data = data.replace("\n", "")
                    # datas = parseMultipleJson(data)
                    r = re.split('(\{.*?\})(?= *\{)', data)
                    # print(data_raw)

                    # print(data)
                    for data in r:
                        if data == '':
                            continue
                        # print(data)
                        json_object = json.loads(data)

                        for k, v in json_object.items():
                            if not k == "FUNC" and 'FUNC' in json_object:
                                update_key_value(json_object['FUNC']+"-"+k, v)
                except UnicodeDecodeError as e:
                    continue
                except JSONDecodeError as je:
                    continue

    except KeyboardInterrupt:
        # print('gg')
        ser.close()    

def draw_line(frame, pt_objects):
    height, width, _ = frame.shape
    if len(pt_objects) > 2:
        x1 = pt_objects[0]['x'] * width
        y1 = pt_objects[0]['y'] * height
        
        x2 = pt_objects[1]['x'] * width
        y2 = pt_objects[1]['y'] * height

        x3 = pt_objects[2]['x'] * width
        y3 = pt_objects[2]['y'] * height
        
        cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)), (0, 0, 255), 2)
        cv2.line(frame, (int(x2), int(y2)), (int(x3), int(y3)), (0, 0, 255), 2)


def draw_object(frame, object):
    # height, width, _ = frame.shape
    # x = object['position']['x'] * width
    # y = object['position']['y'] * height
    # w = object['position']['width'] * width
    # h = object['position']['height'] * height
    # # print(x,y,w,h)
    # x = int(x)
    # y = int(y)
    # w = int(w)
    # h = int(h)
    x,y,w,h = convert_object_to_frame_coordinate(frame, object['p'])


    if 'c' in object:
        cv2.rectangle(frame, (x,y), (x+w, h+y), (255,0,0))
        cv2.putText(frame, str(object['l'])+":"+str(object['c']), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0))
    else:
        
        cv2.rectangle(frame, (x,y), (x+w, h+y), (0,255,0))
        cv2.putText(frame, str(object['t'])+":"+str(object['d']), (x,y+h), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0))


def draw_face(frame, object):
    height, width, _ = frame.shape
    x = object['x'] * width
    y = object['y'] * height
    w = object['w'] * width
    h = object['h'] * height
    # print(x,y,w,h)
    x = int(x)
    y = int(y)
    w = int(w)
    h = int(h)

    # if 'confidence' in object:
        
    #     cv2.rectangle(frame, (x,y), (x+w, h+y), (255,0,0))
    #     cv2.putText(frame, object['label']+":"+str(object['confidence']), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0))
    # else:
        
    #     cv2.rectangle(frame, (x,y), (x+w, h+y), (0,255,0))
    #     cv2.putText(frame, object['type']+":"+str(object['distance']), (x,y+h), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0))
# Calculates Rotation Matrix given euler angles.
def eulerAnglesToRotationMatrix(theta) :

    R_x = np.array([[1,         0,                  0                   ],
                    [0,         math.cos(theta[0]), -math.sin(theta[0]) ],
                    [0,         math.sin(theta[0]), math.cos(theta[0])  ]
                    ])

    R_y = np.array([[math.cos(theta[1]),    0,      math.sin(theta[1])  ],
                    [0,                     1,      0                   ],
                    [-math.sin(theta[1]),   0,      math.cos(theta[1])  ]
                    ])

    R_z = np.array([[math.cos(theta[2]),    -math.sin(theta[2]),    0],
                    [math.sin(theta[2]),    math.cos(theta[2]),     0],
                    [0,                     0,                      1]
                    ])

    R = np.dot(R_z, np.dot( R_y, R_x ))

    return R

def draw(frame, key, value):
    h, w, _ = frame.shape

    if "DMS" in key:
        
        pass
    elif "LDW" in key:
        if 'left' in key or 'right' in key:
            draw_line(frame, value)
        pass
    elif "FCW-Object" in key:
        for obj in value:
            draw_object(frame, obj)
    elif "Tracker-object" in key:
        for obj in value:
            draw_object(frame, obj)

    return

# def process_new_value():
#     global value_dict, mutex
#     if "FCW-frame_no" in value_dict and "Tracker-frame_no" in value_dict and "API-adas_frame":
#         track_fcw_diff = value_dict["Tracker-frame_no"] - value_dict["FCW-frame_no"]
#         input_fcw_diff = value_dict["API-adas_frame"] - value_dict["FCW-frame_no"]
#         ipnut_track_diff = value_dict["API-adas_frame"] - value_dict["Tracker-frame_no"]

#         if len(track_fcw_diff_queue)>30:
#             track_fcw_diff_queue.pop(0)
#         if len(input_track_diff_queue)>30:
#             input_track_diff_queue.pop(0)
#         if len(input_fcw_diff_queue)>30:
#             input_fcw_diff_queue.pop(0)

#         track_fcw_diff_queue.append(track_fcw_diff)
#         input_track_diff_queue.append(ipnut_track_diff)
#         input_fcw_diff_queue.append(input_fcw_diff)
#     if "Tracker-time" in value_dict:
#         if len(track_time_queue)>30:
#             track_time_queue.pop(0)
#         track_time_queue.append(value_dict["Tracker-time"])


#     return

def convert_object_to_frame_coordinate(frame, object):
    height, width, _ = frame.shape
    # print(object)
    x = object['x'] * width
    y = object['y'] * height
    w = object['w'] * width
    h = object['h'] * height
    # print(x,y,w,h)
    x = int(x)
    y = int(y)
    w = int(w)
    h = int(h)
    return x,y,w,h

def show_information(frame, offset, category=None):
    
    global value_dict, mutex
    mutex.acquire()
    try:
        for k,v in value_dict.items():
            if category is not None and category not in k:
                continue
            color = (255,255,255)
            font_size = 1
            if "DMS" in k:
                font_size = 2
            if is_update_in_second(k):
                draw(frame, k, v)
                # if "FCW-Object" not in k:
                # print(k, v)
                cv2.putText(frame, "{}:{}".format(k, v), (0, offset), cv2.FONT_HERSHEY_PLAIN, font_size, color)
            else:
                cv2.putText(frame, "{}:No Update".format(k), (0, offset), cv2.FONT_HERSHEY_PLAIN, font_size, color)
            offset+=20*font_size

            if category == "DMS" and "DMS-main" in value_dict and "DMS-headpose" in value_dict and "DMS-face" in value_dict and is_update_in_second("DMS-main", 500):
                
                face_pos = value_dict['DMS-face']
                headpose = value_dict['DMS-headpose']
                status = value_dict['DMS-main']

                x,y,w,h = convert_object_to_frame_coordinate(frame, face_pos)

                yaw = headpose['yaw']*math.pi/180
                pitch = headpose['pitch']*math.pi/180
                roll = -1*headpose['roll']*math.pi/180
                rotation_matrix = eulerAnglesToRotationMatrix([pitch, yaw, roll])
                cx = np.dot(rotation_matrix, (150,0,0))
                cy = np.dot(rotation_matrix, (0,-150,0))
                cz = np.dot(rotation_matrix, (0,0,130))

                center = (int(x+0.5*w), int(y+0.5*h))
                center = (-1*int(cz[0])+center[0], int(cz[1]*0.5)+center[1])

                cx = (-1*int(cx[0])+center[0], int(cx[1])+center[1])
                cy = (-1*int(cy[0])+center[0], int(cy[1])+center[1])
                cz = (-1*int(cz[0])+center[0], int(cz[1])+center[1])

                cv2.rectangle(frame, (x,y), (x+w, y+h), (255,255,255), 2)
                cv2.arrowedLine(frame, center, cy, (255,0,0), 2)
                cv2.arrowedLine(frame, center, cz, (0,255,0), 2)
                cv2.arrowedLine(frame, center, cx, (0,0,255), 2)

                for key in ["DMS-phone", "DMS-cigratte"]:
                    if is_update_in_second(key, 100):
                        obj = value_dict[key]
                        x,y,w,h = convert_object_to_frame_coordinate(frame, obj)
                        cv2.rectangle(frame, (x,y), (x+w, y+h), (255,255,255), 2)
                        cv2.putText(frame, key.replace("DMS-",""), (x,y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255,255,255))
    except Exception as e:
        print('error',e)

    # if category == "Tracker" and len(track_fcw_diff_queue)>0 and \
    #     len(input_fcw_diff_queue)>0 and len(input_track_diff_queue)>0 and \
    #     len(input_fcw_diff_queue):
    #     cv2.putText(frame, "Track - FCW : {}".format(sum(track_fcw_diff_queue)/len(track_fcw_diff_queue)), (0, offset), cv2.FONT_HERSHEY_PLAIN, 1, color)
    #     offset +=20
    #     cv2.putText(frame, "Input - Track : {}".format(sum(input_track_diff_queue)/len(input_track_diff_queue)), (0, offset), cv2.FONT_HERSHEY_PLAIN, 1, color)
    #     offset +=20
    #     cv2.putText(frame, "Input - FCW : {}".format(sum(input_fcw_diff_queue)/len(input_fcw_diff_queue)), (0, offset), cv2.FONT_HERSHEY_PLAIN, 1, color)
    #     offset +=20
    #     if len(track_time_queue) > 0:
    #         cv2.putText(frame, "Track time : {}".format(sum(track_time_queue)/len(track_time_queue)), (0, offset), cv2.FONT_HERSHEY_PLAIN, 1, color)


    mutex.release()
    return offset


def display_category(frame, offset, category):
    cv2.putText(frame, "------ {} -----".format(category), (0, offset), cv2.FONT_HERSHEY_PLAIN, 1, (255,0,0))
    offset += 20
            
    return offset
    

# def adas_video():
#     global adasOutVideo
#     using_black = False
#     ret = True


#     cv2.namedWindow('adas')

#     if not using_black:
#         cap = cv2.VideoCapture(0)
#         cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
#         cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)    
#         ret, frame_ori = cap.read()
#     else:
#         frame_ori = np.zeros((720,1280,3))

#     while ret and running:
#         ret, frame_ori = cap.read()
#         if not ret:
#             break
#         frame = cv2.resize(frame_ori, (1280,720))
        
#         offset = 100
#         # print('abc')
#         ## process new value
#         process_new_value()


#         ## ADAS
#         offset = display_category(frame, offset, "ADAS")

#         offset = show_information(frame, offset, "LDW")
#         offset = show_information(frame, offset, "FCW")
#         offset = show_information(frame, offset, "Tracker")
        
#         # offset = display_category(frame, offset, "DMS")
#         # offset = show_information(frame, offset, "DMS")
        
#         cv2.imshow('adas', frame)
#         if adasOutVideo is not None:
#             adasOutVideo.write(frame)

#         cv2.waitKey(15)


# def dms_video():
#     global dmsOutVideo
#     using_black = False
#     ret = True    
#     cv2.namedWindow('dms')

#     if using_black:
#         frame_ori = np.zeros((1080,1920,3))
#     else:
#         cap = cv2.VideoCapture(1)
#         cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
#         cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)    
#         ret, frame_ori = cap.read()

#     while ret and running:
#         if not using_black:
#             ret, frame_ori = cap.read()
#         if not ret:
#             break
#         frame = cv2.resize(frame_ori, (1280,720))
        
#         offset = 100

#         ## process new value
#         process_new_value()

#         ## ADAS
#         # offset = display_category(frame, offset, "ADAS")

#         # offset = show_information(frame, offset, "LDW")
#         # offset = show_information(frame, offset, "FCW")
#         # offset = show_information(frame, offset, "Tracker")
        
#         offset = display_category(frame, offset, "DMS")
#         offset = show_information(frame, offset, "DMS")
        
#         cv2.imshow('dms', frame)

#         if dmsOutVideo is not None:
#             dmsOutVideo.write(frame)

#         cv2.waitKey(15)


def do_dms_part(dms_cap):
    frame = None
    ret = True
    if dms_cap is not None:
        ret, frame_ori = dms_cap.read()
        if not ret:
            return ret, None
        frame = cv2.resize(frame_ori, (1280,720))
    else:
        frame = np.zeros((720,1280,3))



    offset = 100

    ## process new value
    # process_new_value()

    offset = display_category(frame, offset, "DMS")
    offset = show_information(frame, offset, "DMS")
    
    if dmsOutVideo is not None:
        dmsOutVideo.write(frame)
    
    return ret, frame

    

def do_adas_part(adas_cap):
    ret, adas_frame_ori = adas_cap.read()
    if not ret:
        return ret, None
    frame = cv2.resize(adas_frame_ori, (1280,720))
    
    offset = 100
    # print('abc')
    ## process new value
    # process_new_value()


    ## ADAS
    offset = display_category(frame, offset, "ADAS")
    offset = show_information(frame, offset, "ReactionTime")
    offset = show_information(frame, offset, "LDW")
    offset = show_information(frame, offset, "FCW")
    offset = show_information(frame, offset, "Tracker")
    
    # if adasOutVideo is not None:
    #     adasOutVideo.write(frame)
    return ret, frame
    



def all_video():
    # global adasOutVideo
    # using_black = True
    # ret = True


    cv2.namedWindow('all')

    adas_cap = cv2.VideoCapture(0)
    adas_cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    adas_cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)    
    adas_ret, _ = adas_cap.read()

    # dms_cap = cv2.VideoCapture(1)
    # dms_cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    # dms_cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)    
    # dms_ret, _ = dms_cap.read()

    while adas_ret and running:
        adas_ret, adas_frame = do_adas_part(adas_cap)
        # dms_ret, dms_frame = do_dms_part(dms_cap)
        
        cv2.imshow('all', adas_frame)
        # cv2.imshow('dms', cv2.resize(dms_frame, (640, 360)))
        cv2.waitKey(10)


adas_video_thread = None
serial_thread = None
dms_video_thread = None
serial_thread = threading.Thread(target = serial_job)
serial_thread.start()
time.sleep(2)
# adas_video_thread = threading.Thread(target = adas_video)
# adas_video_thread.start()
# dms_video_thread = threading.Thread(target = dms_video)
# dms_video_thread.start()


try:
    running = True
    all_video()
    # while True:

        # time.sleep(30)
        # pass
except KeyboardInterrupt:
    running = False
    if serial_thread is not None:
        serial_thread.join()
    if adas_video_thread is not None:
        adas_video_thread.join()
    if dms_video_thread is not None:
        dms_video_thread.join()
    if dmsOutVideo is not None:
        dmsOutVideo.release()
    if adasOutVideo is not None:
        adasOutVideo.release()
    print('exit the program')
    pass